//
//  Item.swift
//  Todoey
//
//  Created by Truong Duc Binh on 6/11/19.
//  Copyright © 2019 bfc. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var isDone: Bool = false
    @objc dynamic var dateCreated: Date?
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
    
}
