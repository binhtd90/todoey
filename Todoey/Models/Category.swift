//
//  Category.swift
//  Todoey
//
//  Created by Truong Duc Binh on 6/11/19.
//  Copyright © 2019 bfc. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    let items = List<Item>()
}

